<?php

return [
    'mode1' => [
        'data' => [
            [
                'id' => 1,
                'first_name' => 'hamid',
                'last_name' => 'kholghi',
                'age' => 26,
                'address' => 'Tehran, Eslamshahr, Motahari Street'
            ],
            [
                'id' => 2,
                'first_name' => 'hamid',
                'last_name' => 'kholghi',
                'age' => 26,
                'address' => 'Tehran, Eslamshahr, Motahari Street'
            ]
        ],
        'mapping' => [
            'id' => [
                'label' => 'شناسه کاربر',
                'type' => 'integer',
                'width' => null,
                'searchable' => false,
                'orderable' => false,
            ],
            'first_name' => [
                'label' => 'نام',
                'type' => 'string',
                'width' => null,
                'searchable' => false,
                'orderable' => false,
            ],
            'last_name' => [
                'label' => 'نام خانوادگی',
                'type' => 'string',
                'width' => null,
                'searchable' => false,
                'orderable' => false,
            ],
            'age' => [
                'label' => 'سن',
                'type' => 'integer',
                'width' => null,
                'searchable' => false,
                'orderable' => false,
            ],
            'address' => [
                'label' => 'آدرس',
                'type' => 'string',
                'width' => null,
                'searchable' => false,
                'orderable' => false,
            ]
        ]
    ],
    'mode2' => [
        'data' => [
            [
                'id' => 2,
                'first_name' => 'naser',
                'last_name' => 'kholghi',
                'age' => 2,
                'address' => [
                    'province' => [
                        'name' => 'qqqqqq'
                    ],
                    'city' => [
                        'name' => 'wwwww'
                    ],
                    'full_address' => 'eeeeee'
                ]
            ],[
                'id' => 2,
                'first_name' => 'naser',
                'last_name' => 'kholghi',
                'age' => 2,
                'address' => [
                    'province' => [
                        'name' => 'qaz'
                    ],
                    'city' => [
                        'name' => 'wsx'
                    ],
                    'full_address' => 'rfv'
                ]
            ],[
                'id' => 2,
                'first_name' => 'naser',
                'last_name' => 'kholghi',
                'age' => 2,
                'address' => [
                    'province' => [
                        'name' => 'sdcsdc'
                    ],
                    'city' => [
                        'name' => 'sdcsdcs'
                    ],
                    'full_address' => 'sdcscsssss'
                ]
            ]
        ],
        'mapping' => [
            'id' => [
                'label' => 'شناسه کاربر',
                'type' => 'integer',
                'width' => null,
                'searchable' => false,
                'orderable' => false,
            ],
            'first_name' => [
                'label' => 'نام',
                'type' => 'string',
                'width' => null,
                'searchable' => false,
                'orderable' => false,
            ],
            'last_name' => [
                'label' => 'نام خانوادگی',
                'type' => 'string',
                'width' => null,
                'searchable' => false,
                'orderable' => false
            ],
            'age' => [
                'label' => 'تاریخ تولد',
                'type' => 'integer',
                'width' => null,
                'searchable' => false,
                'orderable' => false,
                'template' => function ($param) {
                    return \Carbon\Carbon::parse("-$param year")->format('Y');
                }
            ],
            'address' => [
                'label' => 'آدرس',
                'type' => 'string',
                'width' => null,
                'searchable' => false,
                'orderable' => false,
                'template' => function ($param) {
                    return $param['province']['name'];
                }
            ]
        ]
    ],
    'mode3' => [
        'id' => 3,
        'first_name' => 'ali',
        'last_name' => 'kholghi',
        'age' => 33,
        'address' => [
            'province' => [
                'name' => 'Tehran',
                'city' => [
                    'name' => 'eslamshahr'
                ],
                'full_address' => 'kashani Street'
            ]
        ]
    ]
];
