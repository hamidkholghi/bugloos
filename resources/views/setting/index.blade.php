@extends('layout')

@section('content')
    <div class="container-fluid px-1 py-5 mx-auto">
        <div class="row d-flex justify-content-center">
            <div class="col-xl-7 col-lg-8 col-md-9 col-11 text-center">
                <h3>Setting</h3>
                @if (session()->has('message'))
                    <div class="alert alert-info">{{ session()->get('message') }}</div>
                @endif
                <div class="card">
                    <form class="form-card" action="{{ route('settings.store') }}" method="post">
                        @csrf
                        @foreach($setting as $setVal)
                            <div class="row justify-content-between text-left">
                                <div class="form-group col-sm-6 flex-column d-flex">
                                    <label class="form-control-label px-3">{{ $setVal->key }}</label>
                                    <input type="text" id="{{ $setVal->key }}" name="{{ $setVal->key }}"
                                           value="{{ $setVal->value }}">
                                </div>
                            </div>
                        @endforeach
                        <div class="row justify-content-end">
                            <div class="form-group col-sm-6">
                                <button type="submit" class="btn-block btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection