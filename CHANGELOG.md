## 2022-02-19 18:30
- Run ```docker-compose up -d```
- Run ```docker-compose run php-fpm composer install```
- Run ```docker-compose run php-fpm php artisan key:generate```
- Run ```docker-compose run nodejs yarn install && yarn dev```
- Run ```docker-compose run php-fpm php artisan migrate --seed```
- Run ```docker-compose run php-fpm php artisan optimize:clear```
