<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UsersDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        User::firstOrCreate([
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'age' => $faker->dateTime,
            'address' => $faker->address,
            'email' => $faker->email,
            'password' => $faker->password,
        ]);
    }
}
