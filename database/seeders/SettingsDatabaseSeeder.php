<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::firstOrCreate(['key' => 'per_page'], ['value' => '12']);
        Setting::firstOrCreate(['key' => 'table_style'], ['value' => 'table-hover']);
    }
}
