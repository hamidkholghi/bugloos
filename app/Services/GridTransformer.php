<?php

namespace App\Services;

use Illuminate\Pipeline\Pipeline;
use App\Pipelines\TableStyle;
use Illuminate\Support\Arr;
use App\Pipelines\PerPage;

class GridTransformer
{
    private $outputData = [];

    /**
     * @param $data
     * @param $mapping
     * @param null $metas
     * @return array[]
     * @author h.kholghi
     */
    public function output($data, $mapping, $metas = null): array
    {
        $headers = $this->getHeaders($mapping);
        $values = $this->getValues($data, $mapping);
        $metas = $this->getMeta($metas);

        return $this->transformer($headers, $values, $metas);
    }

    /**
     * @param $mapping
     * @return array
     * @author h.kholghi
     */
    private function getHeaders($mapping): array
    {
        return array_values(array_map(function ($mapItem) {
            Arr::forget($mapItem, 'template');
            return $mapItem;
        }, $mapping));
    }

    /**
     * @param $data
     * @param $mapping
     * @return array
     * @author h.kholghi
     */
    private function getValues($data, $mapping): array
    {
        foreach ($data as $index => $selectedData) {
            foreach (array_keys($mapping) as $mapKey) {
                if (isset($mapping[$mapKey]['template']) && is_callable($mapping[$mapKey]['template']))
                    $this->outputData[$index][$mapKey] = call_user_func($mapping[$mapKey]['template'], $selectedData[$mapKey]);
                else
                    $this->outputData[$index][$mapKey] = Arr::get($selectedData, $mapKey);
            }
        }
        return $this->outputData;
    }

    /**
     * @param null $metas
     * @author h.kholghi
     */
    private function getMeta($metas = null)
    {
        return app(Pipeline::class)
            ->send($metas)
            ->through([
                PerPage::class,
                TableStyle::class
            ])
            ->then(function ($data) {
                return $data;
            });
    }

    /**
     * @param $header
     * @param $values
     * @param $metas
     * @return array
     * @author h.kholghi
     */
    private function transformer($header, $values, $metas): array
    {
        return [
            'headers' => $header,
            'values' => $values,
            'meta' => $metas
        ];
    }
}