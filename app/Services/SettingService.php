<?php

namespace App\Services;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Repositories\SettingRepository;
use Illuminate\Support\Collection;

class SettingService
{
    protected $repository;

    /**
     * @param SettingRepository $repository
     * @author h.kholghi
     */
    public function __construct(SettingRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return LengthAwarePaginator|Collection|mixed
     * @author h.kholghi
     */
    public function index()
    {
        return $this->repository->index();
    }

    public function store($request)
    {
        return $this->repository->store($request);
    }

    /**
     * @param $request
     * @return mixed
     * @author h.kholghi
     */
    public function getItemByKey($request)
    {
        return $this->repository->getItemByKey($request);
    }
}