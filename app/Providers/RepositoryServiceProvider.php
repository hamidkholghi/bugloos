<?php

namespace App\Providers;

use App\Repositories\SettingRepositoryEloquent;
use App\Repositories\SettingRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     * @author h.kholghi
     */
    public function register()
    {
        $this->app->bind(SettingRepository::class,
            SettingRepositoryEloquent::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
