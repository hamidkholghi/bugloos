<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Models\Setting;

class SettingRepositoryEloquent extends BaseRepository implements SettingRepository
{
    /**
     * @return string
     * @author h.kholghi
     */
    public function model(): string
    {
        return Setting::class;
    }

    /**
     * @return mixed
     * @author h.kholghi
     */
    public function index()
    {
        return $this->select('key', 'value')->get();
    }

    /**
     * @param $request
     * @author h.kholghi
     */
    public function store($request)
    {
        unset($request['_token']);
        foreach ($request->all() as $key => $value) {
            $item = $this->findByField('key', $key)->first();
            $item->value = $value;
            $item->save();
        }
    }

    /**
     * @param $key
     * @return mixed
     * @author h.kholghi
     */
    public function getItemByKey($key)
    {
        if (!is_null($key['key'])) {
            return $this->findByField('key', $key['key'], ['key', 'value'])->first();
        }
        return $this->select('key', 'value')->get();
    }
}
