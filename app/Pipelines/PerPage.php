<?php

namespace App\Pipelines;

use Illuminate\Pagination\LengthAwarePaginator;
use App\Pipelines\Contracts\PipelineContract;
use App\Services\SettingService;

class PerPage implements PipelineContract
{
    protected $service;

    /**
     * @param SettingService $service
     * @author h.kholghi
     */
    public function __construct(SettingService $service)
    {
        $this->service = $service;
    }

    /**
     * @param $data
     * @param callable $next
     * @return mixed
     * @author h.kholghi
     */
    public function handle($data, $next)
    {
        $current_page = LengthAwarePaginator::resolveCurrentPage();
        if (isset($data['per_page'])) {
            $data['per_page'] = [
                'per_page' => $data['per_page'],
                'current_page' => $current_page,
            ];
        } else {
            $setting_per_page = $this->service->getItemByKey(['key' => 'per_page']);
            $data['per_page'] = [
                'per_page' => $setting_per_page->value,
                'current_page' => $current_page,
            ];
        }

        return $next($data);
    }
}