<?php

namespace App\Pipelines;

use App\Pipelines\Contracts\PipelineContract;
use App\Services\SettingService;

class TableStyle implements PipelineContract
{
    protected $service;

    /**
     * @param SettingService $service
     * @author h.kholghi
     */
    public function __construct(SettingService $service)
    {
        $this->service = $service;
    }

    /**
     * @param $data
     * @param callable $next
     * @return mixed
     * @author h.kholghi
     */
    public function handle($data, $next)
    {
        if (isset($data['table_style'])) {
            $data['table_style'] = [
                'class' => $data['table_style']
            ];
        } else {
            $setting_table_style = $this->service->getItemByKey(['key' => 'table_style']);
            $data['table_style'] = [
                'class' => $setting_table_style->value
            ];
        }

        return $next($data);
    }
}