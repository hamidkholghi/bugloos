<?php

namespace App\Http\Controllers;

use App\Services\GridTransformer;

class GridController extends Controller
{
    protected $gridTransformer;

    /**
     * @param GridTransformer $gridTransformer
     * @author h.kholghi
     */
    public function __construct(GridTransformer $gridTransformer)
    {
        $this->gridTransformer = $gridTransformer;
    }

    /**
     * @return array
     * @author h.kholghi
     */
    public function get_data(): array
    {
        $data = config('constants.mode2.data');
        $mapping = config('constants.mode2.mapping');
        $metas = [
            'per_page' => 10,
            'table_style' => 'table-striped'
        ];

        return $this->gridTransformer->output($data, $mapping, $metas);
    }
}
