<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use App\Http\Requests\StoreSettingRequest;
use App\Http\Requests\GetSettingRequest;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\View;
use App\Services\SettingService;

class SettingController extends Controller
{
    protected $service;

    /**
     * @param SettingService $service
     * @author h.kholghi
     */
    public function __construct(SettingService $service)
    {
        $this->service = $service;
    }

    /**
     * @return Application|Factory|View
     * @author h.kholghi
     */
    public function index()
    {
        $setting = $this->service->index();
            return view('setting.index', compact('setting'));
    }

    /**
     * @param StoreSettingRequest $request
     * @return RedirectResponse
     * @author h.kholghi
     */
    public function store(StoreSettingRequest $request): RedirectResponse
    {
        $this->service->store($request);

        session()->flash('message', 'save settings successfully');
        return redirect()->back();
    }

    /**
     * @param GetSettingRequest $request
     * @return mixed
     * @author h.kholghi
     */
    public function getSettingByKey(GetSettingRequest $request)
    {
        return $this->service->getItemByKey($request);
    }
}
