<?php

use App\Http\Controllers\SettingController;
use App\Http\Controllers\GridController;
use Illuminate\Support\Facades\Route;

Route::controller(GridController::class)->group(function () {
    Route::get('/get-data', 'get_data')->name('get_data');
});

Route::controller(SettingController::class)->prefix('settings')->group(function () {
    Route::get('/', 'index')->name('settings.index');
    Route::get('/get-setting', 'getSettingByKey');
    Route::post('/', 'store')->name('settings.store');
});
